import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); 
		Random rand = new Random(); 
		int number =rand.nextInt(100); 
		int total = 0;

		
		
		int guess ; 
   
         
		do{
            total++;
            System.out.print("Type -1 to quit or give me a guess: ");
            guess = reader.nextInt();
            if(guess != -1)
                System.out.println("Sorry!");
            if (guess == number)
                System.out.println("Congratulations, you guessed at " + total + " attemps");
            else if (guess < number && guess != -1)
                System.out.println("mine is greater than your guess");
            else if (guess > number)
                System.out.println("mine is less than your guess");

        }while(number != guess && guess != -1);
    }
       
}
	
	

